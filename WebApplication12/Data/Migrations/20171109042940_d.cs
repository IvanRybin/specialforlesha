﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WebApplication12.Data.Migrations
{
    public partial class d : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Pic",
                table: "Steps");

            migrationBuilder.AddColumn<string>(
                name: "Pic1",
                table: "Steps",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Pic2",
                table: "Steps",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Pic3",
                table: "Steps",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Pic1",
                table: "Steps");

            migrationBuilder.DropColumn(
                name: "Pic2",
                table: "Steps");

            migrationBuilder.DropColumn(
                name: "Pic3",
                table: "Steps");

            migrationBuilder.AddColumn<string>(
                name: "Pic",
                table: "Steps",
                nullable: true);
        }
    }
}
