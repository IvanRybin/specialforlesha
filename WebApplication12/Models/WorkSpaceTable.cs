﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace WebApplication12.Models
{
    public class WorkSpaceTable
    {
        public int id { get; set; }
        public string name { get; set; }
        public string creator { get; set; }

    }
}
