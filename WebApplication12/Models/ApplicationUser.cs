﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace WebApplication12.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string stringban { get; set; }
        public int age { get; set; }
        public string country { get; set; }
        public List<Comment> CommentLikes { get; set; }
        public List<Instruction> InstructionsRated { get; set; }
        public string admin { get; set; }
        public int Rating { get; set; }
    }
}
