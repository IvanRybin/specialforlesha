﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication12.Models
{
    public class Instruction
    {
        public int Id { get; set; }
        public string Name { get; set; } 
        public string ShortDescription { get; set; }
        public string Creator { get; set; }
        public string Theme { get; set; }
        public string Pic { get; set; }
        public int Rate { get; set; }
        public int FullRate { get; set; }
        public List<Comment> Comments { get; set; }
        public bool Published { get; set; }
        public List<Step> ViewStep;
        public ApplicationUser User;
        public virtual List<Step> Steps { get; set; }
        public string Tags { get; set; }
        public List<String> TagView;
    }
}
