﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication12.Models
{
    
    public class Step
    {
      
        public int Id { get; set; }
        public string Name { get; set; }
        public string StepText { get; set; }
        public string Pic1 { get; set; }
        public string Pic2 { get; set; }
        public string Pic3 { get; set; }
        public int? PostId { get; set; }
        public  Instruction Instruction { get; set; }

    }
}
