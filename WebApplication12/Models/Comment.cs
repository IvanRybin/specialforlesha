﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication12.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Creator { get; set; }
        public int Likes { get; set; }
        public int? PostId { get; set; }
        public Instruction Instruction { get; set; }
        public ApplicationUser User;
    }
}
