﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication12.Models
{
    public class ShakleIds
    {
        public string Id { get; set; }
        public string SomeUserId { get; set; }
        public int SomeInstructionId { get; set; }
        public int SomeCommentId { get; set; }
    }
}
