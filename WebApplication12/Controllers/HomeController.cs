﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication12.Models;
using WebApplication12.Data;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Identity;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace WebApplication12.Controllers
{
   
    public class HomeController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        public HomeController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }
        public async Task<IActionResult> Show()
        {
            // var users = await _userManager.Users.OrderBy(u => u.UserName).ToListAsync();

            return View();
        }
       
        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return Content("file not selected");

            var path = Path.Combine(
                        Directory.GetCurrentDirectory(), "wwwroot"+ @"\Files",
                        file.FileName);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
            var t = new List<string>();
            string g;
            g = file.FileName;
            t.Add(g);
            return View(t);
        }


        [HttpPost]
        public ActionResult AddStep(string inspic,int insid,string theme,string InsName,string shortDescription,string tag)
        {
    
            var option = new DbContextOptionsBuilder<ApplicationDbContext>();
            option.UseSqlServer((@"Server=(localdb)\mssqllocaldb;Database=aspnet-WebApplication12-61D58667-914D-45D6-9852-F4CA2590C63E;Trusted_Connection=True;"));

           var db = new ApplicationDbContext(option.Options);
           var ins = db.Instructions.Where(x => x.Id == insid).First();
            ins.Pic = inspic;
            ins.Theme = theme;
            ins.Name = InsName;
            ins.Tags = tag;
            ins.ShortDescription = shortDescription;

            db.SaveChanges();
           
            return PartialView(ins);
        }
        [HttpPost]
        public ActionResult OpenStep(string id,int insid)
        {

            var option = new DbContextOptionsBuilder<ApplicationDbContext>();
            option.UseSqlServer((@"Server=(localdb)\mssqllocaldb;Database=aspnet-WebApplication12-61D58667-914D-45D6-9852-F4CA2590C63E;Trusted_Connection=True;"));

            var db = new ApplicationDbContext(option.Options);
            var ins = db.Instructions.Where(x => x.Id == insid).First();
            var steps = db.Steps.Where(x => x.PostId == ins.Id).ToList();
            char c = id[0];
            int enume = (int)Char.GetNumericValue(c);
            int retur = enume;
            int thisel = steps.IndexOf(steps[enume]);
            if (id[1] == '-') enume--;
            else enume++;
            Step result = new Step();
            int endof = steps.Count();
            db.SaveChanges();
            if (enume < endof && enume>-1) { result = steps[enume]; result.PostId = enume; }
            else { result = steps[retur]; result.PostId = retur; }

            return PartialView(result);
        }
        [HttpPost]
        public ActionResult AddNextStep(string steppic1,string steppic2,string steppic3,int insid,string stepname,string steptext)
        {
            var option = new DbContextOptionsBuilder<ApplicationDbContext>();
            option.UseSqlServer((@"Server=(localdb)\mssqllocaldb;Database=aspnet-WebApplication12-61D58667-914D-45D6-9852-F4CA2590C63E;Trusted_Connection=True;"));

            var db = new ApplicationDbContext(option.Options);
            var ins = db.Instructions.Where(x => x.Id == insid).First();
            Step step = new Step();
            step.Name = stepname;
            step.StepText = steptext;
            step.PostId = ins.Id;
            step.Pic1 = steppic1;
            step.Pic2 = steppic2;
            step.Pic3 = steppic3;
            step.Instruction = ins;
            if (ins.Steps==null) ins.Steps = new List<Step>();
            ins.Steps.Add(step);
            db.SaveChanges();
            return PartialView(step);
        }

        [HttpPost]
        public ActionResult AddNextStepAndPublish(string steppic1, string steppic2, string steppic3, int insid, string stepname, string steptext)
        {
            var option = new DbContextOptionsBuilder<ApplicationDbContext>();
            option.UseSqlServer((@"Server=(localdb)\mssqllocaldb;Database=aspnet-WebApplication12-61D58667-914D-45D6-9852-F4CA2590C63E;Trusted_Connection=True;"));

            var db = new ApplicationDbContext(option.Options);
            var ins = db.Instructions.Where(x => x.Id == insid).First();
            Step step = new Step();
            step.Name = stepname;
            step.StepText = steptext;
            step.PostId = ins.Id;
            step.Pic1 = steppic1;
            step.Pic2 = steppic2;
            step.Pic3 = steppic3;
            step.Instruction = ins;
            if (ins.Steps == null) ins.Steps = new List<Step>();
            ins.Steps.Add(step);
            ins.Published = true;
            db.SaveChanges();
            return Content("redirect");
        }
        public ActionResult EditIns(string name,string shortdes,string theme,int id)
        {
            var option = new DbContextOptionsBuilder<ApplicationDbContext>();
            option.UseSqlServer((@"Server=(localdb)\mssqllocaldb;Database=aspnet-WebApplication12-61D58667-914D-45D6-9852-F4CA2590C63E;Trusted_Connection=True;"));

            var db = new ApplicationDbContext(option.Options);
            var ins = db.Instructions.Where(x => x.Id == id).First();
            ins.Name = name;
            ins.ShortDescription = shortdes;
            ins.Theme = theme;
            db.SaveChanges();
            return Json("sucess");
        }
        public ActionResult EditStep(string name, string text, int id)
        {
            var option = new DbContextOptionsBuilder<ApplicationDbContext>();
            option.UseSqlServer((@"Server=(localdb)\mssqllocaldb;Database=aspnet-WebApplication12-61D58667-914D-45D6-9852-F4CA2590C63E;Trusted_Connection=True;"));

            var db = new ApplicationDbContext(option.Options);
            var ins = db.Steps.Where(x => x.Id == id).First();
            ins.Name = name;
            ins.StepText = text;
            db.SaveChanges();
            return Json("sucess");
        }


        public ActionResult SearchInstruction(string searchtext)
        {
            var option = new DbContextOptionsBuilder<ApplicationDbContext>();
            option.UseSqlServer((@"Server=(localdb)\mssqllocaldb;Database=aspnet-WebApplication12-61D58667-914D-45D6-9852-F4CA2590C63E;Trusted_Connection=True;"));
            var db = new ApplicationDbContext(option.Options);
            var commentsearch = db.Comments.Where(x => x.Text.Contains(searchtext)).ToList();
            var stepssearch = db.Steps.Where(x => x.StepText.Contains(searchtext)||x.Name.Contains(searchtext)).ToList();
            var instructionsearch= db.Instructions.Where(x => x.Name.Contains(searchtext) || x.ShortDescription.Contains(searchtext)).ToList();
            var greatIdCollection = new List <int>();
            foreach (var item in stepssearch)
            {
                greatIdCollection.Add(Int32.Parse(item.PostId.ToString()));
            }
            foreach (var item in commentsearch)
            {
                greatIdCollection.Add(Int32.Parse(item.PostId.ToString()));
            }
            foreach (var item in instructionsearch)
            {
                greatIdCollection.Add(Int32.Parse(item.Id.ToString()));
            }
            var ids=greatIdCollection.Distinct();
            var ins = new List<Instruction>();
            foreach (var item in ids)
            {
                ins.Add(db.Instructions.Where(x => x.Id == item).First());
            }
            foreach (var item in ins)
            {
                var steps = db.Steps.Where(x => x.PostId == item.Id).ToList();
                var comms = db.Comments.Where(x => x.PostId == item.Id).ToList();
                foreach (var c in comms)
                {
                    c.User = _userManager.FindByIdAsync(c.Creator).Result;
                }
                item.ViewStep = new List<Step>();
                item.ViewStep.AddRange(steps);
                item.TagView = new List<string>();
                string[] tags = { "untaged", "default" };
                if (item.Tags != null)
                {
                    tags = item.Tags.Split();
                    foreach (var t in tags)
                    {
                        if (t != null) item.TagView.Add(t);
                    }
                }
            }
            return View(ins);
        }
        public async Task<ActionResult> DeleteInstruction(string usid,int id)
        {
            var user = await _userManager.FindByIdAsync(usid);
            var option = new DbContextOptionsBuilder<ApplicationDbContext>();
            option.UseSqlServer((@"Server=(localdb)\mssqllocaldb;Database=aspnet-WebApplication12-61D58667-914D-45D6-9852-F4CA2590C63E;Trusted_Connection=True;"));

            var db = new ApplicationDbContext(option.Options);
            var delete = db.Instructions.Where(x => x.Id == id).First();
            var deletecoms = db.Comments.Where(x => x.PostId == id).ToList();
            foreach (var item in deletecoms)
            {
                db.Comments.Remove(item);
            }
            var deletesteps = db.Steps.Where(x => x.PostId == id).ToList();
            foreach (var item in deletesteps)
            {
                db.Steps.Remove(item);
            }
            db.Instructions.Remove(delete);
            db.SaveChanges();
            var ins = db.Instructions.Where(x => x.Creator == user.Id).ToList();
            foreach (var item in ins)
            {
                item.User = _userManager.FindByIdAsync(item.Creator).Result;
                var steps = db.Steps.Where(x => x.PostId == item.Id).ToList();
                var comms = db.Comments.Where(x => x.PostId == item.Id).ToList();
                foreach (var c in comms)
                {
                    c.User = _userManager.FindByIdAsync(c.Creator).Result;
                }
                item.ViewStep = new List<Step>();
                item.ViewStep.AddRange(steps);

            }
            return PartialView(ins);


        }
        public IActionResult Index()
        {
            var option = new DbContextOptionsBuilder<ApplicationDbContext>();
            option.UseSqlServer((@"Server=(localdb)\mssqllocaldb;Database=aspnet-WebApplication12-61D58667-914D-45D6-9852-F4CA2590C63E;Trusted_Connection=True;"));

            var db = new ApplicationDbContext(option.Options);
            var ins = db.Instructions.Where(x => x.Published == true).ToList();
           foreach (var item in ins)
            {
                var steps = db.Steps.Where(x => x.PostId == item.Id).ToList();
                var comms = db.Comments.Where(x => x.PostId == item.Id).ToList();
                foreach (var c in comms)
                {
                    c.User = _userManager.FindByIdAsync(c.Creator).Result;
                }
                item.ViewStep = new List<Step>();
                item.ViewStep.AddRange(steps);
                item.TagView = new List<string>();
                string[] tags = {"untaged", "default" };
               if (item.Tags!=null)
                {
                    tags = item.Tags.Split();
                    foreach (var t in tags)
                    {
                        if (t!=null) item.TagView.Add(t);
                    }
                }
            }
            return View(ins);
        }
        
        public ActionResult Rate(int mark,string insid)
        {
            var option = new DbContextOptionsBuilder<ApplicationDbContext>();
            option.UseSqlServer((@"Server=(localdb)\mssqllocaldb;Database=aspnet-WebApplication12-61D58667-914D-45D6-9852-F4CA2590C63E;Trusted_Connection=True;"));

            var db = new ApplicationDbContext(option.Options);
            var ins = db.Instructions.Where(x => x.Id == Int32.Parse(insid)).First();
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            int middle = db.Hook.Where(x => x.SomeInstructionId == ins.Id).Count();

            if (db.Hook.Where(x => x.SomeInstructionId == ins.Id && x.SomeUserId == user.Id).Count()==0)
            {
                ins.FullRate += mark;
                ins.Rate = ins.FullRate / (middle+1);
                var usinfo = new Hook();
                usinfo.SomeUserId = user.Id;
                usinfo.SomeInstructionId = ins.Id;
                db.Hook.Add(usinfo);
            }
            db.SaveChanges();
            
            return Json("sucess");
        }
        [Microsoft.AspNetCore.Authorization.Authorize]
        public ActionResult CommentLike (string insid)
        {
            var option = new DbContextOptionsBuilder<ApplicationDbContext>();
            option.UseSqlServer((@"Server=(localdb)\mssqllocaldb;Database=aspnet-WebApplication12-61D58667-914D-45D6-9852-F4CA2590C63E;Trusted_Connection=True;"));

            var db = new ApplicationDbContext(option.Options);
            var ins = db.Comments.Where(x => x.Id == Int32.Parse(insid)).First();
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            int middle = db.Hook.Where(x => x.SomeCommentId == ins.Id).Count();

            if (db.Hook.Where(x => x.SomeCommentId == ins.Id && x.SomeUserId == user.Id).Count() == 0)
            {
                ins.Likes++;
                var usinfo = new Hook();
                usinfo.SomeUserId = user.Id;
                usinfo.SomeCommentId = ins.Id;
                db.Hook.Add(usinfo);
            }
            db.SaveChanges();

            return Json("sucess");
        }
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
       
    }
        
}
