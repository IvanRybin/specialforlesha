﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebApplication12.Models;
using WebApplication12.Models.AccountViewModels;
using WebApplication12.Services;
using Microsoft.EntityFrameworkCore;
using WebApplication12.Data;

namespace WebApplication12.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<ApplicationUser> _roleManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ILogger<AccountController> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                // This doesn't count login failures towards account lockout
                //o enable password failures to trigger account lockout, set lockoutOnFailure: true
                if (user.stringban == null)
                {
                    user.stringban = "";


                }
                if (user.stringban == ": banned")
                {

                  _logger.LogWarning("User account locked out.");
                    return RedirectToAction(nameof(Lockout));
                }
                if (user.EmailConfirmed==false)
                {

                    _logger.LogWarning("Not auth");
                    return RedirectToAction("/Home/Index");
                }
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                
                if (result.Succeeded && user.stringban !=": banned")
                {
                    _logger.LogInformation("User logged in.");
                    var users = await _userManager.Users.OrderBy(u => u.UserName).ToListAsync();
                    return RedirectToAction("Index", "Home");
                }
                
                if (result.RequiresTwoFactor)
                {
                    return RedirectToAction(nameof(LoginWith2fa), new { returnUrl, model.RememberMe });
                }
               
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return View(model);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        [HttpGet]
        public async Task<ActionResult> UserPageAsync(string username)
        {
            var user = await _userManager.FindByNameAsync(username);
            var option = new DbContextOptionsBuilder<ApplicationDbContext>();
            option.UseSqlServer((@"Server=(localdb)\mssqllocaldb;Database=aspnet-WebApplication12-61D58667-914D-45D6-9852-F4CA2590C63E;Trusted_Connection=True;"));

            var db = new ApplicationDbContext(option.Options);
            var ins = db.Instructions.Where(x => x.Creator == user.Id).ToList();
            foreach (var item in ins)
            {
                item.User = _userManager.FindByIdAsync(item.Creator).Result;
                var steps = db.Steps.Where(x => x.PostId == item.Id).ToList();
                var comms = db.Comments.Where(x => x.PostId == item.Id).ToList();
                foreach (var c in comms)
                {
                    c.User = _userManager.FindByIdAsync(c.Creator).Result;
                }
                item.ViewStep = new List<Step>();
                item.ViewStep.AddRange(steps);
            }
            var instr = new Instruction();
            instr.Name = "U first instruction";
            instr.User = user;
            if (ins.Count() ==0) ins.Add(instr);
            db.SaveChanges();
            return View(ins);
           


        }

        [HttpGet]
        public async Task<ActionResult> UserPageAsyncShow(string username)
        {
            var user = await _userManager.FindByNameAsync(username);
            var option = new DbContextOptionsBuilder<ApplicationDbContext>();
            option.UseSqlServer((@"Server=(localdb)\mssqllocaldb;Database=aspnet-WebApplication12-61D58667-914D-45D6-9852-F4CA2590C63E;Trusted_Connection=True;"));

            var db = new ApplicationDbContext(option.Options);
            var ins = db.Instructions.Where(x => x.Creator == user.Id && x.Published==true).ToList();
            foreach (var item in ins)
            {
                item.User = _userManager.FindByIdAsync(item.Creator).Result;
                var steps = db.Steps.Where(x => x.PostId == item.Id).ToList();
                var comms = db.Comments.Where(x => x.PostId == item.Id).ToList();
                foreach (var c in comms)
                {
                    c.User = _userManager.FindByIdAsync(c.Creator).Result;
                }
                item.ViewStep = new List<Step>();
                item.ViewStep.AddRange(steps);
            }
            var instr = new Instruction();
            instr.Name = "U first instruction";
            instr.User = user;
            if (ins.Count() == 0) ins.Add(instr);
            db.SaveChanges();
            return View(ins);



        }
        public ActionResult ChangeUsinfo(string usname,int age,string country,string id)
        {
            var user=_userManager.FindByIdAsync(id).Result;
            user.UserName = usname;
            user.age = age;
            user.country = country;
            _userManager.UpdateAsync(user);
            return PartialView(user);
        }
        [AllowAnonymous]
        public ActionResult ShowInstruction(string id)
        {
            var option = new DbContextOptionsBuilder<ApplicationDbContext>();
            option.UseSqlServer((@"Server=(localdb)\mssqllocaldb;Database=aspnet-WebApplication12-61D58667-914D-45D6-9852-F4CA2590C63E;Trusted_Connection=True;"));
            
            var db = new ApplicationDbContext(option.Options);
            var ins = db.Instructions.Where(x => x.Id == Int32.Parse(id)).First();
            var steps = db.Steps.Where(x => x.PostId == ins.Id).ToList();
            var comms = db.Comments.Where(x => x.PostId == ins.Id).ToList();
            foreach (var c in comms)
            {
                c.User = _userManager.FindByIdAsync(c.Creator).Result;
            }
            ins.ViewStep = new List<Step>();
            ins.ViewStep.AddRange(steps);
            return View(ins);
        }

        [HttpGet]
        public ActionResult EditInstruction(string id)
        {
            var option = new DbContextOptionsBuilder<ApplicationDbContext>();
            option.UseSqlServer((@"Server=(localdb)\mssqllocaldb;Database=aspnet-WebApplication12-61D58667-914D-45D6-9852-F4CA2590C63E;Trusted_Connection=True;"));

            var db = new ApplicationDbContext(option.Options);
            var ins = db.Instructions.Where(x => x.Id == Int32.Parse(id)).First();
            var steps = db.Steps.Where(x => x.PostId == ins.Id).ToList();
            var comms = db.Comments.Where(x => x.PostId == ins.Id).ToList();
            foreach (var c in comms)
            {
                c.User = _userManager.FindByIdAsync(c.Creator).Result;
            }
            ins.ViewStep = new List<Step>();
            ins.ViewStep.AddRange(steps);
            return View(ins);
        }
        public ActionResult CreateInstruction()
        {
            var option = new DbContextOptionsBuilder<ApplicationDbContext>();
            option.UseSqlServer((@"Server=(localdb)\mssqllocaldb;Database=aspnet-WebApplication12-61D58667-914D-45D6-9852-F4CA2590C63E;Trusted_Connection=True;"));

            var db = new ApplicationDbContext(option.Options);
            var thisInstruction = new Instruction();
            thisInstruction.Creator = _userManager.FindByNameAsync(User.Identity.Name).Result.Id;
            thisInstruction.Name = "New Instruction";
            thisInstruction.ShortDescription = "Default Description";
            thisInstruction.Rate = 0;
            thisInstruction.Theme = "";
            thisInstruction.Pic = "";
            thisInstruction.Published = false;
            db.Instructions.Add(thisInstruction);
            db.SaveChanges();
            return View(thisInstruction);
        }
        [Authorize]
        public ActionResult AddComment(string comment,int id)
        {
            var option = new DbContextOptionsBuilder<ApplicationDbContext>();
            option.UseSqlServer((@"Server=(localdb)\mssqllocaldb;Database=aspnet-WebApplication12-61D58667-914D-45D6-9852-F4CA2590C63E;Trusted_Connection=True;"));

            var db = new ApplicationDbContext(option.Options);
            var ins = db.Instructions.Where(x => x.Id == id).First();
            Comment addcomment = new Comment();
            addcomment.Likes = 0;
            addcomment.Instruction = ins;
            addcomment.PostId = id;
            addcomment.Text = comment;
            addcomment.Creator = _userManager.FindByNameAsync(User.Identity.Name).Result.Id;
            if (ins.Comments == null) ins.Comments= new List<Comment>();
            if (comment!="empty") ins.Comments.Add(addcomment);
            db.SaveChanges();
            var comms = db.Comments.Where(x => x.PostId == id).ToList();
            foreach(var item in comms)
            {
                item.User = _userManager.FindByIdAsync(item.Creator).Result;
            }
            return PartialView(comms);
        }
        [AllowAnonymous]
        public ActionResult ShowComment(int id)
        {
            var option = new DbContextOptionsBuilder<ApplicationDbContext>();
            option.UseSqlServer((@"Server=(localdb)\mssqllocaldb;Database=aspnet-WebApplication12-61D58667-914D-45D6-9852-F4CA2590C63E;Trusted_Connection=True;"));

            var db = new ApplicationDbContext(option.Options);
            var ins = db.Instructions.Where(x => x.Id == id).First(); 
            if (ins.Comments == null) ins.Comments = new List<Comment>();
            db.SaveChanges();
            var comms = db.Comments.Where(x => x.PostId == id).ToList();
            foreach (var item in comms)
            {
                item.User = _userManager.FindByIdAsync(item.Creator).Result;
            }
            return PartialView(comms);
        }
        [HttpGet]
        public ActionResult WorkSpace()
        {
            var option = new DbContextOptionsBuilder<ApplicationDbContext>();
            option.UseSqlServer((@"Server=(localdb)\mssqllocaldb;Database=aspnet-WebApplication12-61D58667-914D-45D6-9852-F4CA2590C63E;Trusted_Connection=True;"));
             
            var obk = new ApplicationDbContext(option.Options);
            
            return View(obk.WorkTable.ToList());
        }
        [HttpGet]
        public ActionResult AddSpace()
        {
            return View();
        }
        public ActionResult AddSpaceRes(string workname)
        {

            var wb = new WorkSpaceTable();
            wb.creator = User.Identity.Name;
            wb.name = workname;
            var option = new DbContextOptionsBuilder<ApplicationDbContext>();
            option.UseSqlServer((@"Server=(localdb)\mssqllocaldb;Database=aspnet-WebApplication12-61D58667-914D-45D6-9852-F4CA2590C63E;Trusted_Connection=True;"));

            var obk = new ApplicationDbContext(option.Options);
            obk.WorkTable.Add(wb);
            obk.SaveChanges();
            return RedirectToAction("WorkSpace", "Account");
        }
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> LoginWith2fa(bool rememberMe, string returnUrl = null)
        {
            // Ensure the user has gone through the username & password screen first
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();

            if (user == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }

            var model = new LoginWith2faViewModel { RememberMe = rememberMe };
            ViewData["ReturnUrl"] = returnUrl;

            return View(model);
        }
        [HttpGet]
        public async Task<IActionResult> Show()
        {
            var users = await _userManager.Users.OrderBy(u => u.UserName).ToListAsync();
            return PartialView(users);
        }

        [HttpPost]
        public async Task<ActionResult> Deleter(string pak)
        {

            
                var user = await _userManager.FindByEmailAsync(pak);
                await _userManager.DeleteAsync(user);
                
            

            var users = await _userManager.Users.OrderBy(u => u.UserName).ToListAsync();
            return PartialView(users);

        }
        [HttpPost]
        public async Task<ActionResult> Ban(string pak)
        {
            var user = await _userManager.FindByEmailAsync(pak);
            if (user.stringban== ": banned")
            {
                user.stringban= " ";
            }
            else
            {
                user.stringban = ": banned";
            }
           
                await _userManager.UpdateAsync(user);

            var users = await _userManager.Users.OrderBy(u => u.UserName).ToListAsync();
            return PartialView(users);
        }

        [HttpPost]
        public async Task<ActionResult> AdminWork(string pak)
        {
            var user = await _userManager.FindByEmailAsync(pak);
            if (user.admin== "admin")
            {
                user.admin = " ";
            }
            else
            {
                user.admin = "admin";
            }

            await _userManager.UpdateAsync(user);

            var users = await _userManager.Users.OrderBy(u => u.UserName).ToListAsync();
            return PartialView(users);
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginWith2fa(LoginWith2faViewModel model, bool rememberMe, string returnUrl = null)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var authenticatorCode = model.TwoFactorCode.Replace(" ", string.Empty).Replace("-", string.Empty);

            var result = await _signInManager.TwoFactorAuthenticatorSignInAsync(authenticatorCode, rememberMe, model.RememberMachine);

            if (result.Succeeded)
            {
                _logger.LogInformation("User with ID {UserId} logged in with 2fa.", user.Id);
                return RedirectToLocal(returnUrl);
            }
            else if (result.IsLockedOut)
            {
                _logger.LogWarning("User with ID {UserId} account locked out.", user.Id);
                return RedirectToAction(nameof(Lockout));
            }
            else
            {
                _logger.LogWarning("Invalid authenticator code entered for user with ID {UserId}.", user.Id);
                ModelState.AddModelError(string.Empty, "Invalid authenticator code.");
                return View();
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> LoginWithRecoveryCode(string returnUrl = null)
        {
            // Ensure the user has gone through the username & password screen first
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }

            ViewData["ReturnUrl"] = returnUrl;

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginWithRecoveryCode(LoginWithRecoveryCodeViewModel model, string returnUrl = null)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }

            var recoveryCode = model.RecoveryCode.Replace(" ", string.Empty);

            var result = await _signInManager.TwoFactorRecoveryCodeSignInAsync(recoveryCode);

            if (result.Succeeded)
            {
                _logger.LogInformation("User with ID {UserId} logged in with a recovery code.", user.Id);
                return RedirectToLocal(returnUrl);
            }
            if (result.IsLockedOut)
            {
                _logger.LogWarning("User with ID {UserId} account locked out.", user.Id);
                return RedirectToAction(nameof(Lockout));
            }
            else
            {
                _logger.LogWarning("Invalid recovery code entered for user with ID {UserId}", user.Id);
                ModelState.AddModelError(string.Empty, "Invalid recovery code entered.");
                return View();
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Lockout()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.Action(
                        "ConfirmEmail",
                        "Account",
                        new { userId = user.Id, code = code },
                        protocol: HttpContext.Request.Scheme);
                    EmailApp.Services.EmailService emailService = new EmailApp.Services.EmailService();
                    await emailService.SendEmailAsync(model.Email, "Confirm your account",
                        $"Подтвердите регистрацию, перейдя по ссылке: <a href='{callbackUrl}'>link</a>");

                    // await _signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToLocal(returnUrl);
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out.");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult ExternalLogin(string provider, string returnUrl = null)
        {
            // Request a redirect to the external login provider.
            var redirectUrl = Url.Action(nameof(ExternalLoginCallback), "Account", new { returnUrl });
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return Challenge(properties, provider);
        }
        [HttpGet]
        public async Task<ActionResult>  AdminPanel()
        {

            var users = await _userManager.Users.OrderBy(u => u.UserName).ToListAsync();
            return View(users);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            if (remoteError != null)
            {
                ErrorMessage = $"Error from external provider: {remoteError}";
                return RedirectToAction(nameof(Login));
            }
            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return RedirectToAction(nameof(Login));
            }

            // Sign in the user with this external login provider if the user already has a login.
            var user =await  _userManager.FindByLoginAsync(info.LoginProvider,info.ProviderKey);
           if (user.stringban == ": banned")
           {

                _logger.LogWarning("User account locked out.");
                return RedirectToAction(nameof(Lockout));
            }
            var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false, bypassTwoFactor: true);
            if (result.Succeeded)
            {
                _logger.LogInformation("User logged in with {Name} provider.", info.LoginProvider);
                return RedirectToAction("Index", "Home");
            }
            if (result.IsLockedOut)
            {
                return RedirectToAction(nameof(Lockout));
            }
            else
            {
                // If the user does not have an account, then ask the user to create an account.
                ViewData["ReturnUrl"] = returnUrl;
                ViewData["LoginProvider"] = info.LoginProvider;
                var email = info.Principal.FindFirstValue(ClaimTypes.Email);
                return View("ExternalLogin", new ExternalLoginViewModel { Email = email });
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ExternalLoginConfirmation(ExternalLoginViewModel model, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await _signInManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    throw new ApplicationException("Error loading external login information during confirmation.");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await _userManager.AddLoginAsync(user, info);
                    if (result.Succeeded)
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        _logger.LogInformation("User created an account using {Name} provider.", info.LoginProvider);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewData["ReturnUrl"] = returnUrl;
            return View(nameof(ExternalLogin), model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return RedirectToAction(nameof(ForgotPasswordConfirmation));
                }

                // For more information on how to enable account confirmation and password reset please
                // visit https://go.microsoft.com/fwlink/?LinkID=532713
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, Request.Scheme);
                await _emailSender.SendEmailAsync(model.Email, "Reset Password",
                   $"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>");
                return RedirectToAction(nameof(ForgotPasswordConfirmation));
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string code = null)
        {
            if (code == null)
            {
                throw new ApplicationException("A code must be supplied for password reset.");
            }
            var model = new ResetPasswordViewModel { Code = code };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            AddErrors(result);
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }


        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }
}
