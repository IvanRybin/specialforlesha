﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication12.Models;

namespace WebApplication12
{
    public class ChatHub : Hub
    {
        public async Task Send(string message,string roomName)
        {
            await this.Clients.Group(roomName).InvokeAsync("Send", message);
        }
        public async Task SendPic(int xt,int yt)
        {
            await this.Clients.All.InvokeAsync("SendPic",xt,yt);
        }
        public async Task Moving(int xtu, int ytu)
        {
            await this.Clients.All.InvokeAsync("Moving", xtu, ytu);
        }
        public async  Task  AddChatMessage(string roomName)
        {
            await Groups.AddAsync (Context.ConnectionId, roomName);
            this.Clients.Group(roomName).InvokeAsync("AddChatMessage",(Context.ConnectionId + " joined."));
        }

        public  Task LeaveRoom(string roomName)
        {
            return Groups.RemoveAsync(Context.ConnectionId, roomName);
        }
        static List<ApplicationUser> Users = new List<ApplicationUser>();

        // Отправка сообщений
        public async Task SendChat(string message,string name)
        {
            await this.Clients.All.InvokeAsync("Send", message,name);
        }

        // Подключение нового пользователя
       
    }
   
}
