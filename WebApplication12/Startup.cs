﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebApplication12.Data;
using WebApplication12.Models;
using WebApplication12.Services;
using Microsoft.AspNetCore.Http;
using System.Threading;
using Microsoft.Extensions.FileProviders;
using System.IO;

namespace WebApplication12
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
       
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IFileProvider>(
                new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")));

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();
            services.AddAuthentication().AddFacebook(facebookOptions =>
            {
                facebookOptions.AppId = "509720586048381";
                facebookOptions.AppSecret ="0c0bbd7ee549428b872158ff2fd2ff8f";
            });
            services.AddAuthentication().AddTwitter(twitterOptions =>
            {
                twitterOptions.ConsumerKey = "jnCO3U89vpgq1FCKmTUnvmNGk";
                twitterOptions.ConsumerSecret = "U62fczos8rxvHq79vAoQu58PbS4iEX1FSje9ALOxeD7TJ2xk09";
            });
            services.AddAuthentication().AddVkontakte(VkontakteAuthenticationOptions =>
            {
                VkontakteAuthenticationOptions.ClientId = "6257561";
                VkontakteAuthenticationOptions.ClientSecret = "L41HiMeOfLduF720ldrSR";
            });
            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddSignalR();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,IServiceProvider serviceProvider)
        {
          
            


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseSignalR(routes =>
            {
                routes.MapHub<ChatHub>("ShowSpase");
            });
            app.Map("/ws", SocketHandler.Map);

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
